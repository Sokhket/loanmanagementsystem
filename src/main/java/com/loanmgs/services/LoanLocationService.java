package com.loanmgs.services;

import java.util.List;
import java.util.Optional;

import com.loanmgs.models.LoanLocations;

public interface LoanLocationService {

	public List<LoanLocations> getAllLoanLocations();
	public Optional<LoanLocations> getLoanLocationById(int id);
	public Optional<LoanLocations> getLoanLocationByLocation(String location);
	public LoanLocations saveLoanLocation(LoanLocations loanLocations);
	public LoanLocations deleteLoanLocation(int id);
	public LoanLocations updateLoanLocation(LoanLocations loanLocations);
}
