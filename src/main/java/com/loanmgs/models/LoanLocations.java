package com.loanmgs.models;

import java.util.ArrayList;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "TBL_LOAN_LOCATIONS")
public class LoanLocations {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "LOCATION_ID")
	private int id;
	
	@Column(name = "VILLAGE_NAME")
	private String villagename;
	
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name = "IS_ACTIVE")
	private boolean status;
	
	@ManyToMany(mappedBy = "loanLocations", fetch = FetchType.LAZY)
	private List<LoanUsers> loanUsers = new ArrayList<>();
	
	public LoanLocations() {
		
	}
	
	public LoanLocations(int id, String villagename, Date createdDate, boolean status, List<LoanUsers> loanUsers) {
		super();
		this.id = id;
		this.villagename = villagename;
		this.createdDate = createdDate;
		this.status = status;
		this.loanUsers = loanUsers;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVillagename() {
		return villagename;
	}
	public void setVillagename(String villagename) {
		this.villagename = villagename;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public List<LoanUsers> getLoanUser(){
		return loanUsers;
	}
	
	@Override
	public String toString() {
		return "LoanLocations [id=" + id + ", villagename=" + villagename + ", createdDate=" + createdDate + ", status="
				+ status + "]";
	}
	
	
}
