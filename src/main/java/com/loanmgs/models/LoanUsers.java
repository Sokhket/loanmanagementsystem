package com.loanmgs.models;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "TBL_LOAN_USER")
public class LoanUsers {
	
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name = "USER_ID")
	private int id;
	@Column(name = "ROLE")
	private int role;
	@Column(name = "DEFAULT_INTEREST")
	private double defaultInterest;
	@Column(name = "FULL_NAME")
	private String fullName;
	@Column(name = "USERNAME")
	private String userName;
	@Column(name = "PASSWORD")
	private String password;
	@Column(name = "PHONE")
	private String phone;
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.DATE)
	private Date createdDate;
	@Column(name = "IS_BLOCK")
	private boolean isBlock;
	@Column(name = "IS_USER")
	private boolean isUser;
	@Column(name = "IS_SUPER_USER")
	private boolean isSuperUser;
	@Column(name = "IS_LOGIN")
	private boolean isLogin;
	@Column(name = "ACTIVE")
	private boolean isActive;
	
	//@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@ManyToMany
	@JoinTable(name = "TBL_LOAN_USER_LOCATIONS",
			joinColumns = {
					@JoinColumn(name = "FK_USER_ID", referencedColumnName = "USER_ID")},
			inverseJoinColumns = {
					@JoinColumn(name = "FK_LOCATION_ID", referencedColumnName = "LOCATION_ID")}
			)
	//@Column(name = "LOAN_LOCATIONS")
	private List<LoanLocations> loanLocations = new ArrayList<>();
	
	public LoanUsers() {
		super();
	}

	public LoanUsers(int id, int role, double defaultInterest, String fullName, String userName, String password,
			String phone, Date createdDate, boolean isBlock, boolean isUser, boolean isSuperUser, boolean isLogin,
			boolean isActive) {
		super();
		this.id = id;
		this.role = role;
		this.defaultInterest = defaultInterest;
		this.fullName = fullName;
		this.userName = userName;
		this.password = password;
		this.phone = phone;
		this.createdDate = createdDate;
		this.isBlock = isBlock;
		this.isUser = isUser;
		this.isSuperUser = isSuperUser;
		this.isLogin = isLogin;
		this.isActive = isActive;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public double getDefaultInterest() {
		return defaultInterest;
	}

	public void setDefaultInterest(double defaultInterest) {
		this.defaultInterest = defaultInterest;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public boolean isBlock() {
		return isBlock;
	}

	public void setBlock(boolean isBlock) {
		this.isBlock = isBlock;
	}

	public boolean isUser() {
		return isUser;
	}

	public void setUser(boolean isUser) {
		this.isUser = isUser;
	}

	public boolean isSuperUser() {
		return isSuperUser;
	}

	public void setSuperUser(boolean isSuperUser) {
		this.isSuperUser = isSuperUser;
	}

	public boolean isLogin() {
		return isLogin;
	}

	public void setLogin(boolean isLogin) {
		this.isLogin = isLogin;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public List<LoanLocations> getLoanLocations() {
		return loanLocations;
	}

//	public void setLoanLocations(List<LoanLocations> loanLocations) {
//		this.loanLocations = loanLocations;
//	}
		
	
}
