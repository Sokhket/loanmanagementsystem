//package com.loanmgs.models;
//
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//
//@Entity
//@Table(name = "TBL_LOAN_USER_LOCATION")
//public class LoanUserLocation {
//	@Column(name = "USER_ID")
//	private LoanUsers userId;
//	@Column(name = "LOCATION_ID")
//	private LoanLocations locationId;
//	@Column(name = "CREATED_DATE")
//	@Temporal(TemporalType.DATE)
//	private Date createdDate;
//
//	public LoanUserLocation() {
//		
//	}
//	
//	public LoanUserLocation(LoanUsers userId, LoanLocations locationId) {
//		this.userId = userId;
//		this.locationId = locationId;
//	}
//	
//	public LoanUsers getUserId() {
//		return this.userId;
//	}
//	public void setUser(LoanUsers usersId) {
//		this.userId = usersId;
//	}
//	
//	
//	public LoanLocations getLocationsId() {
//		return this.locationId;
//	}
//	public void setLocation(LoanLocations locationId) {
//		this.locationId = locationId;
//	}
//	
//	public Date getCreatedDate() {
//		return this.createdDate;
//	}
//	public void setCreatedDate(Date createdDate) {
//		this.createdDate = createdDate;
//	}
//	
//}
