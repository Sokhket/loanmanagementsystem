package com.loanmgs.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ManiRestController {

	@GetMapping("/")
	public String init() {
		
		return "index";
	}
	
	@PostMapping("/home")
	public String homePage(@RequestParam("username") String username, @RequestParam("password") String pwd, Model model) {
		
		model.addAttribute("username", username);
		model.addAttribute("password", pwd);
		
		return "home";
	}
		
}
