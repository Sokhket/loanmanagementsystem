function ListProcedure() {
		var val = $('#txt_search').val();
		$.ajax({
			url : "search",
			type : "post",
			data : {
				search : val,//name="username"
			},
			dataType : "json",
			error: function(xhr, status, error){
		        var errorMessage = xhr.status + ': ' + xhr.statusText
		        var str2 = "500";
		        if(errorMessage.indexOf(str2) != -1){
		        	location.reload(true);
		        }else{
		        	alert('Error - ' + errorMessage);
		        }
		    },
			success : function(mydata) {
//				var myJSON = JSON.stringify(mydata);
//				alert(mydata);
				
				 $('#table').bootstrapTable('destroy');
				 $('#table').bootstrapTable({
					data : mydata
					 //data: JSON.parse(mydata)
				});
				//alert(JSON.stringify($table.bootstrapTable('getData')))
				
			}
		});
	}
	
	/* $(function() {
		$('#table').bootstrapTable({
			data : mydata
		});
	}); */