<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Panda Login</title>
  
  	<link rel="icon" href="/resources/images/dashboard-img.ico">
    <link rel="stylesheet" href="/resources/css/style.css">    
 
</head>

<body>

  <div class="panda">
  <div class="ear"></div>
  <div class="face">
    <div class="eye-shade"></div>
    <div class="eye-white">
      <div class="eye-ball"></div>
    </div>
    <div class="eye-shade rgt"></div>
    <div class="eye-white rgt">
      <div class="eye-ball"></div>
    </div>
    <div class="nose"></div>
    <div class="mouth"></div>
  </div>
  <div class="body"> </div>
  <div class="foot">
    <div class="finger"></div>
  </div>
  <div class="foot rgt">
    <div class="finger"></div>
  </div>
</div>
<form action="home" method="post" onsubmit="return validation()">
  <div class="hand"></div>
  <div class="hand rgt"></div>
  	<h4>${inv_message}</h4>
  	<h1>Panda Login</h1>
  <div class="form-group">
    <input name="username" required="required" class="form-control"/>
    <label class="form-label">Username</label>
  </div>
  <div class="form-group">
    <input id="password" type="password" name="password" required="required" class="form-control"/>
    <label class="form-label">Password</label>
    <p class="alert">Invalid Credentials..!!</p>
    <button class="btn">Login </button>
  </div>
</form>

 <script  src="/resources/js/login_validation.js"></script>
 <script src='/resources/js/jquery.min.js'></script>
 <script  src="/resources/js/index.js"></script>


</body>

</html>


